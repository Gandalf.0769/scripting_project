import configparser
import logging

logging.basicConfig(
            filename="log.log",
            level=logging.INFO,
            format="%(asctime)s - %(levelname)s - %(message)s",
            datefmt="%d/%m/%Y %I:%M:%S %p",
        )

def generate_default_config():
    """  Generate default config file

    """
    config = configparser.RawConfigParser()
    config.add_section('http')
    config.set('http', 'host', 'tutswiki.com')
    config.set('http', 'ipaddr', '8.8.8.8')
    config.set('http', 'port', '8080')
    config.set('http', 'zip', 'zipname.zip')
    config.set('http', 'file', 'dumpfile')
    config.add_section('smb')
    config.set('smb', 'laptop', 'ISMET-HP-LAPTOP-15')
    config.set('smb', 'sambashare', 'WORKGROUP')
    config.set('smb', 'ip', '127.0.0.1')
    config.set('smb', 'username', 'krnclrismet')
    config.set('smb', 'pwd', 'aze93aze93')
    config.set('smb', 'timetosave', '1')
    config.add_section('email')
    config.set('email', 'send-emails', 'no')
    config.set('email', 'email', "exemple@exemple.com")
    config.set('email', 'pwd', "email_pwd")
    config.set('email', 'smtp', "smtp.mail.yahoo.com")
    config.set('email', 'port', "465")
    config.set('email', 'log-file-attached', "yes")
    config.set('email', 'title', "Hello Word")
    config.set('email', 'destination', "first@email.com")
    with open('config.ini', 'w') as conf:
        config.write(conf)


def get_info(config, section_name, key):
    """ Extract information from ini file

    :param config: ini file loc, section_name: name of a section, key: value of a key
    :type config: string, section_name: string,key: ?
    :return value: string
    """
    config_object = configparser.ConfigParser()
    config_object.read(config)
    distantserv = config_object[section_name]
    return distantserv[key]


def update_info(section_name, key, new_value):
    """ Update information from ini file

    :param config: ini file loc, section_name: name of a section, key: value of a key
    :type config: string, section_name: string,key: ?
    :return value: string
    """
    try:
        config_object = configparser.ConfigParser()
        config_object.read("./config.ini")
        section = config_object[section_name]
        if section[key]:
            section[key] = str(new_value)
            with open('./config.ini', 'w') as conf:
                config_object.write(conf)
    except KeyError:
        logging.error("key is incorrect")
    except Exception as e:
        logging.error(e)
