import requests

class Mattermost:
    """Interact with mattermost telecom server.

    :Methods:
        :members
            logs_all:  LogEmailMattermost -> log  file , email, mattermost notification
            url_notif: string -> url to send notification
            matt_content: dict -> notification
        :methods
            info(task): get info
            error(task): get error info
            format_to_array(): Set data to mattermost format to be send as an array.
            send_mattermost_notification(): send notification to mattermost telecom server.
    """
    def __init__(self, logs):
        """ Constructor

        :param logs: object to manage with all logs
        :type log: object
        :return
        """

        self.logs_all = logs

        self.url_notif = (
             'https://chat.telecomste.fr/hooks/13ta3nw3e787tnxxqkyxfh5xnr'
        )

        # Initialize mattermost notification content.
        self.matt_content = {
            "tasks": [
                " Progress             ",
                ":-------------------------------",  # 30 dashes
            ],
            "state": [" State              ", ":-------------------"],
        }

    def info(self, task):
        """ Get info on current status

        :param task: task to ad to matttermost notification
        :type task: string
        :return
        """
        max_size = len(self.matt_content["tasks"][1])
        for i in range(max_size - len(task) + 1):
            # Add the difference of spaces between max size and task size.
            task += " "
        # Add the task to tasks array
        self.matt_content["tasks"].append(task)
        # Add the state to states array
        self.matt_content["state"].append(" :white_check_mark: ")

    def error(self, task):
        """ Get info on current status

        :param task: task to ad to matttermost notification
        :type task: string
        :return
        """

        max_size = len(self.matt_content["tasks"][1])
        for i in range(max_size - len(task) + 1):
            # Add the difference of spaces between max size and task size.
            task += " "
        # Add the task to tasks array
        self.matt_content["tasks"].append(task)
        # Add the state to states array
        self.matt_content["state"].append(" :no_entry:         ")

    def format_to_array(self):
        """ Set data to mattermost format to be send as an array.

        :param
        :type
        :return formatted_data: string
        """

        formatted_data = ""

        for i in range(len(self.matt_content["tasks"])):
            formatted_data += (
                "|"
                + self.matt_content["tasks"][i]
                + "|"
                + self.matt_content["state"][i]
                + "|\n"
            )
        return formatted_data

    def send_mattermost_notification(self):
        """ Send notification to mattermost telecom server

        :param
        :type
        :return
        """
        # Format data to be prepared to be sent.
        formatted_data = self.format_to_array()

        payload = {
            "channel": "inforx-scripts",
            "icon_url": "https://www.mattermost.org/wp-content/uploads/2016/04/icon.png",
            "text": formatted_data,
        }

        try:
            # Post request on Mattermost TSE server
            req = requests.post(self.url_notif, json=payload, verify=False)
            # Raise error if request was not accepted.
            req.raise_for_status()
        except requests.exceptions.ConnectionError:
            self.logs_all.warning(
                "Mattermost notification:",
                "Notification not sent. Connection error, please check your mattermost chat is still runinng.",
            )

        except requests.exceptions.HTTPError as err:
            self.logs_all.warning(
                "Mattermost notification:",
                "Notification not sent. HTTP error code: " + err.args[0],
            )

        except requests.exceptions.Timeout:
            self.logs_all.warning(
                "Mattermost notification:",
                "Notification not sent. Timeout, please retry.",
            )

        except requests.exceptions.RequestException:
            self.logs_all.warning(
                "Mattermost notification:", "Unknown error.")
