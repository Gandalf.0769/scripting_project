# -*- coding: utf-8 -*-

import datetime
import os
from smb.SMBConnection import SMBConnection
import config as conf

class Smb_server:

    """ Manage operations to send files to smb server.

    members: 
        notification_sender: Notification_sender -> manage all logs
        time_to_save: int -> Time that takes to save data on the server
        ip: string -> IP of smb server
        user: string -> user's username
        pwd: string -> user's password
        is_date_ok: boolean -> compare last modification's date and today's date

    methods: 
    connect(action): Connect to smb server with ssh keys.
    send_to_smb_server(tgz_name_): Send tgz file to smb server.
    archival_check(): Check and remove old tgz files depending on the time to save files.
    check_file_ack(tgz_name_): Check if smb server has tgz file.
    close(): Close smb connection if this one is still opened.
    """

    def __init__(self, config_, logging_, is_date_ok_):
        """Constructor
        :param config_: data of all server info; logging: for logs;
            is_date_ok_: compare last modification's date and today's date
        """

        self.notification_sender = logging_

        self.user = conf.get_info(config_, "smb", "username")
        self.pwd = conf.get_info(config_, "smb", "pwd")
        self.ip = conf.get_info(config_, "smb", "ip")

        self.is_date_ok = is_date_ok_

        try:
            time_to_save_ = int(conf.get_info(
                config_, "smb", "timetosave"))
        except ValueError:
            time_to_save_ = 10
            self.notification_sender.warning("Ini Error",
                                             "Error while reading. Default value is 10 days.",)
        self.time_to_save = time_to_save_

        self.smb = None

        self.smbshare = conf.get_info(config_, "smb", "smbshare")
        self.laptop = conf.get_info(config_, "smb", "laptop")



    def connect(self, action):
        """Connect to smb server
        :param action: Current action
        :type action: string
        :returns smb: smb server
        """

        try:

            self.smb = SMBConnection(self.user, self.pwd, self.laptop, self.smbshare, use_ntlm_v2 = True)
            self.smb.connect(self.ip, 139)

            self.notification_sender.info(
                "action",
                "Connection succeded with smb server at" + self.ip,
            )
            return True


        except IOError:
            self.notification_sender.error(
                "smb connection",
                "Error while connecting to smb server",
            )

    def send_to_smb_server(self, tgz_name_):
        """Send tgz file to smb server.
        :param tgz_name_: tgz file (tar.gz) to send
        """

        try:
            if self.smb is not None:
                file  = open("./" + tgz_name_,"rb")
                self.smb.storeFile(self.smbshare,"/data/" + tgz_name_, file)
                os.remove(tgz_name_)

                self.notification_sender.info("Envoie au serveur SMB")

        except (IOError, OSError) as err:
            if err is OSError:
                path = "Local"
            else:
                path = "Remote"
                self.notification_sender.error(
                    "Envoie au serveur SMB", path + " path does not exists.")

        finally:
            if self.smb is not None:
                self.close()

    def is_archived(self):
        """Check and remove old tgz files
        """

        try:
            if self.connect("ack") is not None:
                # Current directory.
                files = self.smb.listPath(self.smbshare,"/data")
                dead_line = (
                    datetime.datetime.today()
                    - datetime.timedelta(days=self.time_to_save)
                ).date()
                for file in files:
                    try:
                        date = datetime.datetime.strptime(
                            file.filename.replace(".tgz", ""), "%Y%d%m"
                        ).date()
                        if date < dead_line:
                            self.smb.deleteFiles(self.smbshare,"/data/" + file.filename)
                        self.notification_sender.info("Comparaison des fichiers")

                    except ValueError:
                        pass

            else:
                self.notification_sender.error(
                    "smb archival", "Connection to smb server failed."
                )
                self.notification_sender.error(
                    "Send to smb", "Sending to smb server failed."
                )
                self.notification_sender.error("Connexion au serveur SMB", "Checking ack failed.")

        except IOError:
            self.notification_sender.error(
                "smb archival", "Path does not exist.")

        finally:
            if not self.is_date_ok and self.smb is not None:
                self.notification_sender.warning(
                    "Send to smb",
                    "Sending to smb server failed.",
                )
                self.notification_sender.warning(
                    "ack", "Checking ack failed."
                )
                self.close()

    def is_file_exist(self, tgz_name_):
        """Check on smb server if file exist
        :param tgz_name_: check on smb server if file exist
        :type tgz_name_: string
         """
    
        try:
            if (self.connect("ack")) :
                    files = self.smb.listPath(self.smbshare,"/data")
                    for file in files:
                        if file == tgz_name_:
                            self.notification_sender.info(
                                "Fichier recu sur le serveur SMB", "Tgz file sent to smb server.")
                            return

        except IOError:
            self.notification_sender.error(
                "ack", "Remote path does not exists.")

        finally:
                self.close()

    def close(self):
        """Close smb connection.
        """

        if self.smb is not None:
            self.smb.close()
