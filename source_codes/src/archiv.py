
import logging
import tarfile
import os;
from zipfile import ZipFile;

logging.basicConfig(
            filename="log.log",
            level=logging.INFO,
            format="%(asctime)s - %(levelname)s - %(message)s",
            datefmt="%d/%m/%Y %I:%M:%S %p",
        )


def get_all_file(directory):
    """Get all file from the directory
    :param directory : directory 
    :type : string
    :return file_path : string
    """
    file_paths = []
    for root, directories, files in os.walk(directory+'.'):
        for filename in files:
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)
    return file_paths     

def zip_file(file,directory):
    """To zip files
    :param file : file to unzip; directory : directory
    :type file: object; directory : string
    """
    try:
        file_paths= get_all_file(directory)
        if file_paths:
            with ZipFile(file+'.zip','w') as zip:
                print('Zipping all the files ')
                for files in file_paths:
                    zip.write(files)
                print('Succeed')
        else:
            raise Exception("The folder is empty");
    except Exception:
        logging.error(Exception)
        print(Exception)

def unzip_file(file):
    """To unzip files
    :param file : file to unzip
    :type file: object
    """
    try:
        with ZipFile(file,'r') as zip:
            zip.printdir()
            print('Unzipping all the files now')
            zip.extractall()
            print('Succeed')
    except Exception :
        logging.error(Exception)
        print(Exception)

def tar_file(file,directory):
    """To compress files in tar
    :param file : file to unzip; directory : directory
    :type file: object; directory : string
    """
    try:
        file_paths= get_all_file(directory)
        if file_paths:
            with tarfile.open(file+'.tar.gz', "w:gz") as tar:
                print('Compressing all the files now')
                for files in file_paths:
                    tar.add(files)
                print('Succeed')
        else:
            raise Exception("The folder is empty");
    except Exception:
        logging.error(Exception)
        print(Exception)


def untar_file(file):
    """To untar files
    :param file : file to unzip
    :type file: object
    """
    try:
        with tarfile.open(file+'.tar.gz','r:gz') as tar:
            print('Extracting all the files now...')
            tar.extractall();
            print('Done!')
    except Exception:
        logging.error(Exception)
        print(Exception)

