import os
import archiver as arch
import smb_server

def main():
    archive = None
    archive = arch.Archiver(8000, "./config.ini")
    archive.get_config()
    # if arc.critical_nb > 0:
        # return
    archive.get_zip()
    archive.extract_zip()
    smb = None
    if archive.iszipfile():
        archive.compress_to_tgz()
        # File date is not the same than before - file has been changed today
        is_date_ok = archive.compare_date()
        # smb server area.

        smb = smb_server.Smb_server("./config.ini", archive.notification_sender, is_date_ok)

        smb.is_archived()

        if is_date_ok:
            smb.send_to_smb_server(archive.tgz_name)
            smb.is_file_exist(archive.tgz_name)

    archive.notification_sender.send_all()
    archive.clean(smb)

if __name__ == "__main__":
    main()