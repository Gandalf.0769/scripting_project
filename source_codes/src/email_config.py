# -*- coding: utf-8 -*-

import re
import smtplib
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email import encoders
from email_validator import validate_email
import config as conf

class Email_config:
    """Manage e-mail.

    :Methods:
        :members
            server_connexion:  object -> stmp connection
            email_content: array of strings -> Content of sent emails
            max_int: int -> number of problem occured during  compil
            logs_all: LogEmailMattermost -> log, email, mattermost notification
            ip: string -> email server ip adress
            port: int -> port used fo connection to server  
            auth: dict -> Get identification to server (username & password)
            log_file_attached: string -> To know if user want to attach log file
            title: string -> email title
            destination: array of string -> all email adress
        :methods
            login(): Logging-in mail server
            smtp_server(): Create a secure connection to SMTP server
            login_and_send(): Log-in to e-mail account and send e-mail to all destinations
            internal_server(): Create a connection ton internal email server and send emails through it
            add_content(message_type, message, time): Add content to email body
            def attachment(msg): Attach log file
            format_data(): Format content
            send_email(): If there is no ip in config file, then use internal server
    """

    def __init__(self, config_, logs):
        """ Email instance

        :param  json:  Contains all e-mail parameters in config file; 
                logs: Object to manage all logs.
        :type json: dict; logs: NotificationSender
        :return
        """
        self.server_connexion = None
        self.email_content = []

        self.max_int = 0

        self.logs_all = logs

        try:
            auth_ = {
                "email": conf.get_info(config_, "email", "email"),
                "password": conf.get_info(config_, "email", "pwd")
            }
            log_file_attached_ =  conf.get_info(config_, "email", "log-file-attached")
            title_ =  conf.get_info(config_, "email", "title")
            destination_ =  conf.get_info(config_, "email", "destination")
            port_ =  conf.get_info(config_, "email", "port")
            self.ip =  conf.get_info(config_, "smb", "ip")

        except KeyError as err:
            self.logs_all.warning(
                "Getting email config",
                "Missing keyword '"
                + err.args[0]
                + "' in email bracket in config file. Try again please.",
            )
            self.max_int += 1
            return

        if (
                not re.fullmatch(r"(\d{1,3}.){3}\d{1,3}", self.ip)
                and self.ip.split(".")[0] != "smtp"
        ):
            self.ip = ""

        try:
            self.port = int(port_)
        except ValueError:
            self.port = 465
            self.logs_all.warning(
                "Getting email config",
                "Error with email port. Default value is 465.",
            )

        if auth_["email"] == "":
            self.logs_all.warning(
                "Getting email config",
                "Error with email id. Email not sent.",
            )

        if auth_["password"] == "":
            self.logs_all.warning(
                "Getting email config",
                "Error with email password. Email not sent.",
            )

        if log_file_attached_ not in ("yes", "no", "y", "n"):
            log_file_attached_ = "yes"
            self.logs_all.warning(
                "Getting email config",
                "Error in log-file-attached value. Yes by default",
            )

        if title_ == "":
            title_ = "HelloWord"
            self.logs_all.warning(
                "Getting email config",
                "No title for email, 'HelloWord' by default",
            )

        if len(destination_) == 0:
            self.logs_all.warning(
                "Getting email config", 
                "No destination email.Please choose at least one."
            )
        else:
            for email_dest in destination_:
                if not validate_email(email_dest):
                    self.logs_all.warning(
                        "Getting email config", email_dest + " is not valid."
                    )
                    destination_.remove(email_dest)

        self.auth = auth_
        self.log_file_attached = log_file_attached_
        self.title = title_
        self.destination = destination_

    def login(self):
        """Logging-in mail server.

        """
        try:
            self.server_connexion.login(self.auth["email"], self.auth["password"])

        except smtplib.SMTPAuthenticationError:
            self.logs_all.warning(
                "Logging-in email server",
                "Error detected with username/password. Emails not sent.",
            )

        except smtplib.SMTPNotSupportedError:
            self.logs_all.warning(
                "Logging-in email server",
                "The authentification command is not supported. Emails not sent.",
            )

        except smtplib.SMTPException:
            self.logs_all.warning(
                "Logging-in email server",
                "Unknown error occured while logging-in. Emails not sent.",
            )

    def login_and_send(self):
        """Log-in to e-mail account and send e-mail to all destinations.

        """

        try:
            self.login()

            msg = MIMEMultipart()

            msg["Subject"] = self.title

            msg.attach(MIMEText(self.format_data(), "plain"))

            msg = self.attachment(msg)

            self.server_connexion.sendmail(
                self.auth["email"], self.destination, msg.as_string())

            self.logs_all.info("Sending emails")

            if self.server_connexion is not None:
                self.server_connexion.quit()

        except smtplib.SMTPRecipientsRefused:
            self.logs_all.warning(
                "Sending emails",
                "Nobody had received emails. Error occured",
            )

        except smtplib.SMTPSenderRefused:
            self.logs_all.warning(
                "Sending emails",
                "Wrong email address",
            )

        except smtplib.SMTPDataError:
            self.logs_all.warning(
                "Sending emails",
                "Unexpected error  with the server",
            )

        except smtplib.SMTPServerDisconnected:
            self.logs_all.warning(
                "Sending emails",
                "Server disconnected",
            )

        # Another error
        except smtplib.SMTPException:
            self.logs_all.warning(
                "Sending emails",
                "Unknown error occured",
            )

    def smtp_server(self):
        """ Create a secure connection to SMTP server.

        """

        context = ssl.create_default_context()

        try:
            self.server_connexion = smtplib.SMTP_SSL(
                self.ip, port=self.port, context=context
            )

        except TimeoutError:
            self.logs_all.warning(
                "Email server connection",
                "Timeout error",
            )

        except smtplib.SMTPServerDisconnected:
            self.logs_all.warning(
                "Email server connection",
                "Server disconnected",
            )

        if self.server_connexion is not None:
            self.login_and_send()

    def internal_server(self):
        """Create a connection ton internal email server and send emails through it.
            domain : "localhost.com",
            password : "35279155",
            account : "admin@localhost.com",
            password : "admin"
        """

        try:
            with smtplib.SMTP("localhost") as server:
                server.login("admin", "admin")
                msg = MIMEMultipart()
                msg["Subject"] = "Object"
                server.sendmail(
                    "script@localhost.com", [
                        "krnclr.ismet@gmail.com"], msg.as_string()
                )
        except Exception:
            pass

    def add_content(self, message_type, message, time):
        """ Add content to email body.
        
        :param message_type : info/error; message : content; time : time to send
        :type message_type : string; message : string; time : string
        """
        self.email_content.append(time + " - " + message_type + ": " + message)

    def attachment(self, msg):
        """Attach log file
        :param msg: content with the attach file
        :type msg: object
        :return msg: object
        """
        if self.log_file_attached in ("yes", "y"):
            try:
                log_filename = self.logs_all.get_logfile()

                mime_instance = MIMEBase("application", "octet-stream")

                with open(log_filename, "rb") as attachment:

                    mime_instance.set_payload(attachment.read())

                    encoders.encode_base64(mime_instance)

                    mime_instance.add_header(
                        "Content-Disposition", "attachment; filename= " + log_filename
                    )

                    msg.attach(mime_instance)

                    self.logs_all.info("Attachment")

            # Catching opening file exception.
            except EnvironmentError:
                self.logs_all.warning(
                    "Attachment",
                    "Unknown error occured during mail attachment."
                )

        return msg

    def format_data(self):
        """Format content
        :return formatted_data: string
        """

        formatted_data = ""
        for line in self.email_content:
            formatted_data += line + "\n"
        return formatted_data

    def send_email(self):
        """if there is no ip in config file, then use internal server
        """

        if self.ip != "":
            self.smtp_server()
        else:
            self.internal_server()
