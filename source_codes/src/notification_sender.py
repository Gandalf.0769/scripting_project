# -*- coding: utf-8 -*-

import os
import logging
from datetime import datetime
from mattermost import Mattermost
from email_config import Email_config


class Notification_sender:
    """ Manage all communication (send notification)
        :members
            error_number: int -> Number of errors occured while processing.
            warning_number: int -> Number of warnings occured while processing.
            mattermost: Mattermost -> Object mattermost notification.
            email: Email -> Object manage emails.
            send_notification: string -> Inform if mattermost notification has to be send.
            send_emails: string ->  inform if emails have to be send.
        :methods
            info(current_action, message=None): Add information to all log files, mattermost notification and email content
            warning(current_action, message=None): Create warning to inform user
            error(current_action, message=None): Add error to all log files, mattermost notification and email content
            critical(current_action, message=None): Add critical status to all log files
            get_logfile(): Get log filename
            send_all(): Send email and mattermost notification.
    """

    def __init__(self, email_config=None, notification=None):
        """Initialize log file, email content and notification content.
        :param email_config:  data to succeed email sending; notification: inform if notification has to be sent
        :type email_config: dic; notification: string
        """

        self.error_number = 0
        self.warning_number = 0

        logging.basicConfig(
            filename="log.log",
            level=logging.INFO,
            format="%(asctime)s - %(levelname)s - %(message)s",
            datefmt="%d/%m/%Y %I:%M:%S %p",
        )

        self.mattermost = None
        self.send_notification = None

        if notification is not None:
            self.send_notification = notification
            self.mattermost = Mattermost(self)
            if notification not in ("always", "error", "never"):
                self.send_notification = "always"
                self.warning(
                    "config.ini read",
                    "Error in setting notification parameters. Default value is always.",
                )

        self.email = None
        self.send_emails = None

        if email_config is not None:
            self.email = Email_config(email_config, self)

            if self.email.max_int > 0:
                self.email = None
                return

            try:
                self.send_emails = email_config["send-emails"].lower()
                if self.send_emails not in ("yes", "y", "no", "n"):
                    self.send_emails = "yes"
                    self.warning(
                        "config.ini read",
                        "Sending email format not supported. Default value is Yes.",
                    )

            except KeyError as err:
                self.warning(
                    "Getting email config",
                    "Missing keyword '"
                    + err.args[0]
                    + "' error in config.ini file. Please try again",
                )

    def info(self, current_action, message=None):
        """ Add information to all log files, mattermost notification and email content
        :param current_action: status; message: add message
        :type current_action: string; message: string
        """

        if message is not None:
            logging.info("%s: %s", current_action, message)
        else:
            logging.info('Task: "%s" went good.', current_action)

        if self.mattermost is not None:
            self.mattermost.info(current_action)

        now = datetime.now().strftime("%H:%M:%S")
        if self.email is not None:
            if message is not None:
                self.email.add_content("INFO", current_action + ": " + message, now)
            else:
                self.email.add_content(
                    "INFO", '"' + current_action + '" occured properly.', now
                )

    def warning(self, current_action, message=None):
        """ Create warning to inform user
        :param current_action: status; message: add message
        :type current_action: string; message: string
        """

        self.warning_number += 1

        if message is not None:
            logging.warning("%s: %s", current_action, message)
        else:
            logging.warning('Task: "%s" raised a warning.', current_action)

        now = datetime.now().strftime("%H:%M:%S")
        if self.email is not None:
            if message is not None:
                self.email.add_content("WARNING", current_action + ": " + message, now)
            else:
                self.email.add_content(
                    "WARNING", '"' + current_action + '" raised a warning.', now
                )

    def error(self, current_action, message=None):
        """ Add error to all log files, mattermost notification and email content
        :param current_action: status; message: add message
        :type current_action: string; message: string
        """

        self.error_number += 1

        if message is not None:
            logging.error("%s: %s", current_action, message)
        else:
            logging.error('Task: "%s" failed.', current_action)

        if self.mattermost is not None:
            self.mattermost.error(current_action)

        now = datetime.now().strftime("%H:%M:%S")
        if self.email is not None:
            if message is not None:
                self.email.add_content("ERROR", current_action + ": " + message, now)
            else:
                self.email.add_content(
                    "ERROR", '"' + current_action + '" did not occured properly.', now
                )

    def critical(self, current_action, message=None):
        """ Add critical status log files
        :param current_action: status; message: add message
        :type current_action: string; message: string
        """

        if message is not None:
            logging.critical("%s: %s", current_action, message)
        else:
            logging.critical('Task: "%s" failed.', current_action)

    def get_logfile(self):
        """Get log filename
        :returns log_name: string -> name of the current log file
        """

        return str(
            os.path.basename(logging.getLoggerClass().root.handlers[0].baseFilename)
        )

    def send_all(self):
        """Send email and mattermost notification.
        """

        if self.mattermost is not None and self.send_notification is not None:
            if (
                    self.send_notification == "always"
                    or self.send_notification == "error"
                    and self.error_number > 0):
                self.mattermost.send_mattermost_notification()

        if self.send_emails is not None and self.email is not None:
            if self.send_emails in ("yes", "y"):
                self.email.send_email()

        if self.error_number > 0 and self.warning_number > 0:
            msg = (
                "Not succed with "
                + str(self.error_number)
                + " errors and "
                + str(self.warning_number)
                + " warnings."
            )
        elif self.error_number > 0:
            msg = "Not succed with " + str(self.error_number) + " errors."
        elif self.warning_number > 0:
            msg = "Succed with " + str(self.warning_number) + " warnings."
        else:
            msg = "Succed"

        if self.error_number > 0:
            logging.error(msg)
        else:
            logging.info(msg)
