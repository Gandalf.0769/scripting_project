import datetime
import io
import os
import zipfile
import urllib3

import notification_sender as ns
import config as conf

class Archiver:
    """Handle compressinng operation
    :members
        max_int: int -> number of problem occured during  compil
        config: string -> config filename
        port: int -> port used to connect to web server
        file_name: string -> file name
        zip_name: string -> name of the zip file
        tgz_name: string -> name of the tgz file 
        zip_file: Zipfile object -> zip file 
        time_to_save: int -> time to push on smb server
        notification_sender: object -> object to manage log file, mattermost notification and email
    :methods 

    Methods
    -------
    get_config(): Get configurations from config file and make sure that values are availables.
    get_zip(): Make GET HTTP request to get zip file on web server.
    create_zip(data): Create zip file from data got from web server.
    compare_date():Get last modified date of file (name) and zip (zfile) and compare with 
    today's date. If it is the same it means file has been change earlier
    today.
    extract_zip(): Extract zip.
    has_file(): Check if zip contains dump file.
    compress_to_tgz(): Compress file to .tgz.
    clean(smb): Clean all the folder, and closing all connections still opened
    """

    def __init__(self, port_, config_):
        """Constructor
        :param port_: http port; config_: config file name
        :type port_: int; config_path_: string
        """
        self.config = config_
        self.port = port_

        self.zip_file = None
        self.zip_name = None

        self.tgz_name = datetime.datetime.now().date().strftime("%Y%d%m") + ".tgz"

        self.file_name = None

        self.time_to_save = None

        self.notification_sender = None

        self.get_config()

    def get_config(self):
        """get config
        """
        self.notification_sender = ns.Notification_sender(None, "always")
        self.zip_name = conf.get_info(self.config, "http", "zip")
        self.file_name = conf.get_info(self.config, "http", "file")

        try:
            time_to_save_ = int(conf.get_info(self.config, "smb", "timetosave"))
        except ValueError:
            time_to_save_ = 10
            self.notification_sender.warning("Ini Error",
                                             "error while reading Time to save value format is not supported. Default value is 10 days.",)
        self.time_to_save = time_to_save_

    def get_zip(self):
        """ get zip file from http server
        """
        http = urllib3.PoolManager()
        req = None
        try:
            url = "http://localhost:" + \
                str(self.port) + "/Web-serv/" + self.zip_name
            req = http.request("GET", url)
            if req.status == 404:
                raise urllib3.exceptions.ResponseError()
            self.create_zip(req.data)

        except urllib3.exceptions.ConnectTimeoutError:
            self.notification_sender.error(
                "Connection Timeout error. Please try again.")
        except urllib3.exceptions.ConnectionError:
            self.notification_sender.error("Request ZIP", "Connection refused.")
        except urllib3.exceptions.ResponseError:
            self.notification_sender.error(
                "Request ZIP", self.zip_name+" not found")
        except urllib3.exceptions.RequestError:
            self.notification_sender.error("Request ZIP", "Connection refused.")
        except urllib3.exceptions.TimeoutError:
            self.notification_sender.error("Request ZIP", "Cannot connect.")
        except urllib3.exceptions.SSLError:
            self.notification_sender.error("Request ZIP", "SMB failed")

    def create_zip(self, data):
        """create zip file from bytestream
        :param data: request response
        :type  data: Bytestream
        """

        try:
            self.zip_file = zipfile.ZipFile(io.BytesIO(data), "r")
        except (zipfile.BadZipFile, zipfile.LargeZipFile):
            self.zip_file = None

    def compare_date(self):
        """Get last modified date then compare with today's date.
        :returns boolean -> false if date!= today 
        """

        if self.zip_file is None:
            return False

        infos = self.zip_file.infolist()
        i = 0
        file_date = None
        for i, _ in enumerate(infos):
            if infos[i].filename == self.file_name:
                file_date = datetime.datetime(*infos[i].date_time[0:3])

        if file_date is not None:
            same_date = file_date.date() == datetime.datetime.now().date()
            if same_date:
                self.notification_sender.warning(
                    "Compare dates", "Modification dates are the same."
                )
            else:
                self.notification_sender.info(
                    "Compare dates", "Modification dates are differents."
                )
            return same_date

        return False

    def iszipfile(self):
        """check if the zip file has dumb file
        :return boolean -> true if unzip = file & false if unzip !=file
        """
        if self.zip_file is not None:
            for file in self.zip_file.infolist():
                if file.filename == self.file_name:
                    self.notification_sender.info(
                        "Dump present", "Zip contain "+self.file_name)
                    return True
                self.notification_sender.error(
                    "Dump present", "Zip has not file "+self.file_name)
                return False
        else:
            self.notification_sender.error(
                "Dump present", "Zip is empty")
            return False

    def extract_zip(self):
        """ Unzip
        """

        if self.zip_file is None:
            self.notification_sender.error(
                "UnZip succeeded", "ZIP file does not exist.")
            return
        try:
            self.zip_file.extractall(os.getcwd())
            self.notification_sender.info("Fichier Zip extrait")

        except zipfile.BadZipFile:
            self.notification_sender.error(
                "UnZip succeeded", " ZIP file not extracted."
            )

        except zipfile.LargeZipFile:
            self.notification_sender.error(
                "UnZip succeded",
                "ZIP file not extracted.")
        finally:
            self.zip_file.close()

    def compress_to_tgz(self):
        """compress a file to tgz
        """
        err = os.system(
            'tar -czf "'
            + os.getcwd()
            + "/"
            + self.tgz_name
            + '" "'
            + self.file_name
            + '"')
        if err != 0:
            self.notification_sender.error(
                "Compress file", "Fail to compress " + self.file_name + "file.")
        if os.path.exists(os.getcwd() + "/" + self.file_name):
            os.remove(os.getcwd() + "/" + self.file_name)

    def clean(self, smb):
        """Clean all the folder
        :param smb: smb server
        :type smb: Samba object
        """

        if os.path.exists(os.getcwd() + "/" + self.tgz_name):
            os.remove(os.getcwd() + "/" + self.tgz_name)
        if self.zip_file is not None:
            for file in self.zip_file.infolist():
                if os.path.exists(os.getcwd() + "/" + file.filename):
                    os.remove(os.getcwd() + "/" + file.filename)

        if smb is not None:
            smb.close()
