#!/bin/bash

python3 --version

sudo apt-get install --upgrade python3

sudo apt-get install python3-pip

python3 -m pip --version

python3 -m pip install email_validator
python3 -m pip install urllib3
python3 -m pip install psutil

#creer le fichier crontab

{ crontab -l -u $USER; echo "0 23 * * * cd $PWD; python3 main.py"; } | crontab -u $USER -