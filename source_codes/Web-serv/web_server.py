import atexit
import http.server
import logging
import os
import socketserver
import threading
import psutil

HOST_NAME = "localhost"
PORT = 8000
logging.basicConfig(
            filename="log.log",
            level=logging.INFO,
            format="%(asctime)s - %(levelname)s - %(message)s",
            datefmt="%d/%m/%Y %I:%M:%S %p",
        )


def start():
    """Start web server
    """
    handler = http.server.SimpleHTTPRequestHandler
    try:
        with socketserver.TCPServer(("", PORT), handler) as httpd:
            logging.error("Server started at http://localhost:%s", str(PORT))
            httpd.serve_forever()
    except Exception as e:
        print("Error while launching project " + e)


def start_service():
    """Start a service
    """
    download_thread = threading.Thread(target=start, name="launcher")
    download_thread.start()
    print("http://localhost:" + str(PORT) + " | OK")
    # Launch web server as a service.
    os.system("python3 -m http.server " + str(PORT))


def stop_service():
    """stop a service
    """

    pid_list = psutil.net_connections()
    if len(pid_list) == 0:
        return
    pid = []
    for connection in pid_list:
        if (connection.laddr[1] == PORT and connection.status in ("ESTABLISHED", "LISTEN")):
            pid.append(connection.pid)
    for process in pid:
        os.system("taskkill /F /PID " + str(process))


if __name__ == "__main__":
    start_service()
    atexit.register(stop_service)
